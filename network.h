#ifndef NETWORK_H
#define NETWORK_H

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Tworzy nową pustą sieć i zwraca jej identyfikator
 *  Sieć pusta, to sieć z pustym zbiorem węzłów.
 *  Parametr growing mówi o tym, czy nowa sieć 
 *  ma być rosnąca (growing != 0)
 *  czy nie (growing == 0)
 */
unsigned long network_new(int growing);

/** Jeżeli istnieje sieć o identyfikatorze id,
 *  to usuwa sieć, wpp. nic nie robi.
 */
void network_delete(unsigned long id);

/** Jeżeli istnieje sieć o identyfikatorze id,
 *  zwraca liczbę jej węzłów, wpp. zwraca 0.
 */
size_t network_nodes_number(unsigned long id);

/** Jeżeli istnieje sieć o identyfikatorze id, 
 *  zwraca liczbę jej krawędzi, wpp. zwraca 0.
 */
size_t network_links_number(unsigned long id);

/** Jeżeli istnieje sieć o identyfikatorze id, 
 *  label != NULL i sieć ta nie zawiera jeszcze 
 *  węzła o etykiecie label, to dodaje węzeł 
 *  o etykiecie label do sieci, wpp. nic nie robi.
 */ 
void network_add_node(unsigned long id, const char* label);

/** Jeżeli istnieje sieć o identyfikatorze id, 
 *  slabel != NULL oraz tlabel != NULL, 
 *  i sieć ta nie zawiera jeszcze krawędzi (slabel, tlabel), 
 *  to dodaje krawędź (slabel, tlabel) do sieci, 
 *  wpp. nic nie robi. Jeżeli w sieci nie istnieje węzeł 
 *  o etykiecie któregoś z końców krawędzi, 
 *  to jest on również dodawany.
 */
void network_add_link(unsigned long id, const char* slabel, 
const char* tlabel);

/** Jeżeli istnieje sieć o identyfikatorze id, 
 *  a w niej węzeł o etykiecie label oraz sieć 
 *  nie jest rosnąca, to usuwa węzeł z sieci 
 *  wraz ze wszystkimi krawędziami wchodzącymi 
 *  i wychodzącymi, wpp. nic nie robi.
 */
void network_remove_node(unsigned long id, const char* label);

/** Jeżeli istnieje sieć o identyfikatorze id, 
 *  a w niej krawędź (slabel, tlabel), oraz sieć 
 *  nie jest rosnąca, to usuwa krawędź z sieci,
 *  wpp. nic nie robi.
 */
void network_remove_link(unsigned long id, const char* slabel, 
const char* tlabel); 
 
/** Jeżeli istnieje sieć o identyfikatorze id 
 *  i sieć nie jest rosnąca, usuwa wszystkie jej węzły 
 *  i krawędzie, wpp. przypadku nic nie robi.
 */
void network_clear(unsigned long id);

/** Jeżeli istnieje sieć o identyfikatorze id, 
 *  a w niej węzeł o etykiecie label, to zwraca 
 *  liczbę krawędzi wychodzących z tego węzła, 
 *  wpp. zwraca 0.
 */
size_t network_out_degree(unsigned long id, const char* label);

/** Jeżeli istnieje sieć o identyfikatorze id, 
 *  a w niej węzeł o etykiecie label, to zwraca 
 *  liczbę krawędzi wchodzących do tego węzła, 
 *  wpp. zwraca 0.
 */
size_t network_in_degree(unsigned long id, const char* label);

#ifdef __cplusplus
}
#endif

#endif
