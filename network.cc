#include <iostream>
#include <cassert>
#include "network.h"
#include "growingnet.h"

#include <map>
#include <set>
#include <utility>

#ifdef DEBUG 
const bool debug = true; 
#else 
const bool debug = false;
#endif

typedef std::set<std::string> t_edge;
typedef std::pair<t_edge, t_edge> t_edges;
typedef std::map<std::string, t_edges> t_nodes;
typedef std::pair<bool, t_nodes> t_network;
typedef std::map<unsigned long, t_network> t_networks;

unsigned long int nr = 0;
t_networks& networks()
{
    static t_networks netw;
    return netw;
}

bool network_exists(unsigned long id);
void debug_network_exists(std::string action);
void debug_growing_network(std::string action);
void debug_node_exists(std::string action);
void debug_link_exists(std::string action);
void debug_proper_label(std::string action);

/** Tworzy nową pustą sieć i zwraca jej identyfikator
 *  Sieć pusta, to sieć z pustym zbiorem węzłów.
 *  Parametr growing mówi o tym, czy nowa sieć 
 *  ma być rosnąca (growing != 0)
 *  czy nie (growing == 0)
 */
unsigned long network_new(int growing)
{
    if (debug)
    {
        std::cerr << __func__ << "(" << growing << ")" << std::endl;
    }
    if (growing != 0)
    {
        networks().insert(std::make_pair(nr, t_network(true, t_nodes())));
    }
    else
    {
        networks().insert(std::make_pair(nr, t_network(false, t_nodes())));
    }
    if (debug)
    {
        std::cerr << __func__ << ": network " << nr << " created" << std::endl;
    }
    nr++;
    return  nr - 1;
}

/** Jeżeli istnieje sieć o identyfikatorze id,
 *  to usuwa sieć, wpp. nic nie robi.
 */
void network_delete(unsigned long id)
{
    if (debug)
    {
        std::cerr << __func__ << "(" << id << ")" << std::endl;
        if (!network_exists(id)) 
            debug_network_exists("delete");
    }
    


    size_t erased = networks().erase(id);

    if (debug)
    {
        assert(networks().count(id) == 0);
        if (erased > 0)
            std::cerr << __func__ << ": network " << id << 
                " erased" << std::endl;
    }
}

/** Jeżeli istnieje sieć o identyfikatorze id,
 *  zwraca liczbę jej węzłów, wpp. zwraca 0.
 */
size_t network_nodes_number(unsigned long id)
{
    if (debug)
    {
        std::cerr << __func__ << "(" << id << ")" << std::endl;
    }
    if (network_exists(id))
    {
        t_network& network = networks()[id];
        t_nodes& netNodes = network.second;
        if (debug)
        {
            std::cerr << __func__ << ": network " << id <<
                    " contains " << netNodes.size() << " nodes"
                    << std::endl;
        }
        return netNodes.size();
    }
    return 0;
}

/** Jeżeli istnieje sieć o identyfikatorze id, 
 *  zwraca liczbę jej krawędzi, wpp. zwraca 0.
 */
size_t network_links_number(unsigned long id)
{
    if (debug)
    {
        std::cerr << __func__ << "(" << id << ")" << std::endl;
    }

    if (!network_exists(id))
    {
        if (debug) debug_network_exists("count links of");
        return 0;
    }
    
    size_t links_number = 0;
    t_network& network = networks()[id];
    t_nodes& nodes = network.second;
    
    for (auto it = nodes.begin(); it != nodes.end(); ++it)
    {
        t_edges& edges = it->second;
        links_number += edges.first.size();
        links_number += edges.second.size();
        if (debug) 
        {
            std::cerr << __func__ << ": node " << it->first << 
                " has " << edges.first.size() + edges.second.size() <<
                " coincident edges" << std::endl;
        }
    }

    links_number /= 2;
    
    if (debug)
    {
        std::cerr << __func__ << ": network " << id <<
            " contains " << links_number << " link(s)" << std::endl;
    }

    return links_number;
}

/** Jeżeli istnieje sieć o identyfikatorze id, 
 *  label != NULL i sieć ta nie zawiera jeszcze 
 *  węzła o etykiecie label, to dodaje węzeł 
 *  o etykiecie label do sieci, wpp. nic nie robi.
 */ 
void network_add_node(unsigned long id, const char* label)
{
    if (debug)
    {
        std::cerr << __func__ << "(" << id << ", " << label << ")" << std::endl;
    }
    if ((label != NULL) && (network_exists(id)))
    {
        t_network& network = networks()[id];
        t_nodes& netNodes = network.second;
        t_nodes::iterator iter = netNodes.find(label);
        if (iter == netNodes.end())
        {
            netNodes.insert(std::make_pair(label, t_edges()));
            if (debug)
            {
                std::cerr << __func__ << ": network " << id << ", node "
                        << label << " added" << std::endl;
            }
        }
    }
}

/** Jeżeli istnieje sieć o identyfikatorze id, 
 *  slabel != NULL oraz tlabel != NULL, 
 *  i sieć ta nie zawiera jeszcze krawędzi (slabel, tlabel), 
 *  to dodaje krawędź (slabel, tlabel) do sieci, 
 *  wpp. nic nie robi. Jeżeli w sieci nie istnieje węzeł 
 *  o etykiecie któregoś z końców krawędzi, 
 *  to jest on również dodawany.
 */
void network_add_link(unsigned long id, const char* slabel, 
const char* tlabel)
{
    if (debug)
    {
        std::cerr << __func__ << "(" << id << "," << slabel <<
            "," << tlabel << ")" << std::endl;
    }
    
    if (!network_exists(id))
    {
        if (debug) debug_network_exists("add a link to");
        return;
    }

    if (slabel == NULL || tlabel == NULL) 
    {
        if (debug) debug_proper_label("add a link to");
        return;
    }
    
    t_network& network = networks()[id];
    t_nodes& nodes = network.second;
    t_edge& out_edge = nodes[slabel].second;
    t_edge& in_edge = nodes[tlabel].first;
    out_edge.insert(tlabel);
    in_edge.insert(slabel);

    if (debug)
    {
        assert(nodes.count(slabel) > 0);
        assert(nodes.count(tlabel) > 0);
        assert(out_edge.count(tlabel) > 0);
        assert(in_edge.count(slabel) > 0);
        std::cerr << __func__ << ": link (" << slabel << 
            ", " << tlabel << ") added to network " <<
            id << std::endl;
    }
}

/** Jeżeli istnieje sieć o identyfikatorze id, 
 *  a w niej węzeł o etykiecie label oraz sieć 
 *  nie jest rosnąca, to usuwa węzeł z sieci 
 *  wraz ze wszystkimi krawędziami wchodzącymi 
 *  i wychodzącymi, wpp. nic nie robi.
 */
void network_remove_node(unsigned long id, const char* label)
{
    if (debug)
    {
        std::cerr << __func__ << "(" << id << ", " << label << ")" << std::endl;
    }
    if ((label != NULL) && (network_exists(id)) && (!networks()[id].first))
    { // sieć istnieje i nie jest rosnąca
        t_nodes& netNodes = networks()[id].second;
        if (netNodes.find(label) != netNodes.end())
        { // istnieje węzeł
            t_edge& inEdges = networks()[id].second[label].first;
            t_edge& outEdges = networks()[id].second[label].second;
            if (debug)
            {
                std::cerr << __func__ << ": in network " << id << ", removed node " 
                        << label << " in-edges: ";
            }
            for(auto it : inEdges) 
            { // usuwamy wchodzące do niego krawędzie z innych węzłów 
                if (debug)
                {
                    std::cerr << it << "-" << label << ", ";
                }
                netNodes[it].second.erase(netNodes[it].second.find(label));
            }
            if (debug)
            {
                std::cerr << std::endl << " and out-edges: ";
            }
            for(auto it : outEdges) 
            { // usuwamy wychodzące z niego krawędzie do innych węzłów
                if (debug)
                {
                    std::cerr << label << "-" << it << ", ";
                }
                netNodes[it].first.erase(netNodes[it].first.find(label));
            }
            if (debug)
            {
                std::cerr << std::endl;
            }
            inEdges.clear();
            outEdges.clear();
            // usuwamy węzeł
            netNodes.erase(netNodes.find(label));
            if (debug)
            {
                std::cerr << __func__ << ": network " << id << ", node " 
                        << label << " removed" << std::endl;
            }
        }
    }
}

/** Jeżeli istnieje sieć o identyfikatorze id, 
 *  a w niej krawędź (slabel, tlabel), oraz sieć 
 *  nie jest rosnąca, to usuwa krawędź z sieci,
 *  wpp. nic nie robi.
 */
void network_remove_link(unsigned long id, const char* slabel, 
const char* tlabel)
{
    if (debug)
    {
        std::cerr << __func__ << "(" << id << "," << slabel <<
            "," << tlabel << ")" << std::endl;
    }

    if (!network_exists(id))
    {
        if (debug) debug_network_exists("remove a link from");
        return;
    }
   
    t_network& network = networks()[id];
    
    //jeśli rosnąca
    if (network.first)
    {
        if (debug) debug_growing_network("remove a link from");
        return; 
    }

    t_nodes& nodes = network.second;

    if (nodes.count(slabel) < 1 || nodes.count(tlabel) < 1)
    {
        if (debug) debug_link_exists("remove");
        return;
    }

    t_edge& out_edge = nodes[slabel].second;
    t_edge& in_edge = nodes[tlabel].first;
    size_t erased = 0;
    erased += out_edge.erase(tlabel);
    erased += in_edge.erase(slabel);

    if(debug)
    {
        assert(out_edge.count(tlabel) == 0);
        assert(in_edge.count(slabel) == 0);
        if (erased > 0)
            std::cerr << __func__ << ": link (" << slabel << ", " <<
                tlabel << ") removed from network " << id << std::endl;
        else
            debug_link_exists("remove");
    }
}
 
/** Jeżeli istnieje sieć o identyfikatorze id 
 *  i sieć nie jest rosnąca, usuwa wszystkie jej węzły 
 *  i krawędzie, wpp. przypadku nic nie robi.
 */
void network_clear(unsigned long id)
{
    if (debug)
    {
        std::cerr << __func__ << "(" << id << ")" << std::endl;
    }
    if (network_exists(id) && (!networks()[id].first))
    { // istnieje sieć id i nie jest rosnąca
        network_delete(id);
        networks().insert(std::make_pair(id, t_network(false, t_nodes())));
        if (debug)
        {
            std::cerr << __func__ << ": network " << id << " cleared"
                    << std::endl;
        }
    }
}

/** Jeżeli istnieje sieć o identyfikatorze id, 
 *  a w niej węzeł o etykiecie label, to zwraca 
 *  liczbę krawędzi wychodzących z tego węzła, 
 *  wpp. zwraca 0.
 */
size_t network_out_degree(unsigned long id, const char* label)
{   
    if (debug)
    {
        std::cerr << __func__ << "(" << id << "," << label << 
            ")" << std::endl;
    }

    if (!network_exists(id))
    {
        if (debug) debug_network_exists("count out-edges of");
        return 0;
    }

    t_network& network = networks()[id];
    t_nodes& nodes = network.second;
    
    if (nodes.count(label) == 0)
    {
        if (debug) debug_node_exists("count out-edges of");
        return 0;
    }

    t_edge& out_edge = nodes[label].second;
    size_t edge_count = out_edge.size();

    if (debug)
    {
        std::cerr << __func__ << ": in network " << id <<
            " node " << label << " has " << edge_count <<
            " out-edges" << std::endl;
    }

    return edge_count;
}

/** Jeżeli istnieje sieć o identyfikatorze id, 
 *  a w niej węzeł o etykiecie label, to zwraca 
 *  liczbę krawędzi wchodzących do tego węzła, 
 *  wpp. zwraca 0.
 */
size_t network_in_degree(unsigned long id, const char* label)
{
    if (debug)
    {
        std::cerr << __func__ << "(" << id << ", " << label << 
            ")" << std::endl;
    }
    if ((!network_exists(id)))
    { //nie istnieje sieć o takim id
        return 0;
    }
    
    t_network& network = networks()[id];
    t_nodes& nodes = network.second;
    
    if((nodes.count(label) < 1))
    { //nie istnieje węzeł
        return 0;
    }
    
    t_edge& in_edges = nodes[label].first;
    
    if (debug)
    {
        std::cerr << __func__ << ": in network " << id <<
            " node " << label << " has " 
            << in_edges.size() <<
            " in-edges" << std::endl;
    }
    return in_edges.size();
}

/** Czy istnieje sieć o identyfikatorze id.
 */
bool network_exists(unsigned long id)
{
    return (networks().find(id) != networks().end());
}

/*
 * DEBUG FUNCTIONS
 */

void debug_network_exists(std::string action)
{
    std::cerr << "attempt to " << action << 
        " a non-existent network" << std::endl;
}

void debug_growing_network(std::string action)
{
    std::cerr << "attempt to " << action << 
        " a growing network" << std::endl;
}

void debug_node_exists(std::string action)
{
    std::cerr << "attempt to " << action << 
        " a non-existent node" << std::endl;
}

void debug_link_exists(std::string action)
{
    std::cerr << "attempt to " << action << 
        " a non-existent link" << std::endl; 
}

void debug_proper_label(std::string action)
{
    std::cerr << "attempt to " << action <<
        " a node with null label" << std::endl;
}


