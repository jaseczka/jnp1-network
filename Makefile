CXX = g++
CXXFLAGS = -Wall -std=c++11

ifeq ($(debuglevel), 1)
	CXXFLAGS += -Werror -pedantic -DDEBUG -g
else
	CXXFLAGS += -O2
endif

.PHONY: all clean

all: growingnet.o network.o

growingnet.o: growingnet.cc growingnet.h

network.o: network.cc network.h growingnet.h

%.o: %.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	rm -rf *.o
	
